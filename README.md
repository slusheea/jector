# Jector

Easily create new projects from custom templates

![Usage](/images/usage.gif)

### Create a new project 

Use the following syntax to create a new project 

```bash
jector [TEMPLATE_NAME] [PROJECT_NAME]
```

### Add custom templates

You can use git repositories or local folders as templates.
If you use both, the program will prefer the git repository.

To generate the default config you can run:
```bash
jector new config
```
Note: Jector will only attempt to create a new config file if one doesn't already exist.
If you run `jector new config` and the config file already exists, it will attempt to create a project named "config" from a template named "new"

To add templates you can edit the config file:

`.config/jector/jector.toml`
```toml
shell = "nu" # Defaults to sh if not defined
# This will clone the repo
[[template]]
name = "your_custom_template_name"
git = "https://git_template.com/your_template.git"

# This will copy a folder and run some predefined scripts
[[template]]
name = "another_custom_template_name"
dir = "/home/username/path/to/the/template/folder"
scripts = ["my_script_name", "my_second_script"]

# Adds a predefined script
[[script]]
name = "my_script_name"
run = "/path/to/script.sh"

[[script]]
name = "my_second_script"
run = "you/get/the/idea"
```

Or use jector to create a new one:
```bash
jector -g [GIT_URL] [TEMPLATE_NAME]
jector -d [TEMPLATE_PATH] [TEMPLATE_NAME]

# You can use the -s flag to create a template with a script
jector -g [GIT_URL] [TEMPLATE_NAME] -s [SCRIPT_NAME]
# (use the -s flag multiple times to define multiple scripts)

# You can also predefine scripts
jector -p [SCRIPT_NAME] [SCRIPT_PATH]
```

### Building from source

To build jector you'll need to have rust installed. [Instructions here](https://www.rust-lang.org/tools/install)

Run the following commands:
```
~ » git clone https://gitlab.com/slusheea/jector.git
~ » cd jector 
~ » cargo +nighly build --release
~ » sudo cp target/release/jector /usr/bin
```

### Installing a binary

Instructions to install the binary in the [releases](https://gitlab.com/slusheea/jector/-/releases) page
