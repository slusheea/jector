use std::{
    collections::HashMap,
    env::current_dir,
    fs::{copy, create_dir, create_dir_all, read_to_string, File, OpenOptions},
    io::{ErrorKind, Write},
    path::{Path, PathBuf},
    process::Command,
};

use clap::Parser;
use git2::Repository;
use serde_derive::Deserialize;
use walkdir::WalkDir;

const DEFAULT_CONFIG: &str = "# Custom configuration should look like this:
#
# shell = \"sh\" # Defaults to sh when not defined.
#                # Can be used to set non POSIX compliant shells like fish or nu.
# 
# [[template]]
# name = \"template_name\"
# git = \"https://git.repo/link\"
# dir = \"/home/username/path/to/template/folder\"
# scripts = [\"script_name\", \"other_script\"]
#
# [[script]]
# name = \"script_name\"
# run = \"/home/username/path/to/script.sh\
";

// Holds [[template]] and [[script]]
#[derive(Deserialize)]
pub struct Config {
    pub shell: String,
    pub template: Option<Vec<Template>>,
    pub script: Option<Vec<Script>>,
}

// Holds the contents of [[template]]
#[derive(Deserialize)]
pub struct Template {
    pub name: String,
    pub git: Option<String>,
    pub dir: Option<String>,
    pub scripts: Option<Vec<String>>,
}

// Holds the contents of [[script]]
// If the user tries to access a predef script and the `run` part of that template is missing,
// jector will panic attempting to access a non-existant value on a hashmap.
#[derive(Deserialize)]
pub struct Script {
    pub name: String,
    pub run: String,
}

#[derive(Parser)]
#[clap(author, version, about, long_about = None)]
pub struct Cli {
    // jector [TEMPLATE_NAME] [PROJECT_NAME]
    /// Name of the template
    pub temp: String,

    /// Name of the project
    pub name: String,

    /// Create new git template
    #[clap(short)]
    pub git: bool,

    /// Create new local directory template
    #[clap(short)]
    pub dir: bool,

    /// Create a new predefined script
    #[clap(short)]
    pub predef: bool,

    /// Add a script to the new template
    #[clap(short, value_parser)]
    pub scripts: Option<Vec<String>>,
}

pub enum Create {
    Project,
    GitTemp,
    DirTemp,
    PredefScript,
    None,
}

pub enum Project {
    Git(String),
    Dir(String),
    None,
}

fn config_dir_path() -> PathBuf {
    let mut config_dir_path = home::home_dir().unwrap();
    config_dir_path.push(".config");
    config_dir_path.push("jector");
    config_dir_path
}

pub fn config_file_path() -> PathBuf {
    let mut config_file_path = config_dir_path();
    config_file_path.push("jector.toml");
    config_file_path
}

fn target_project_path(project_name: &str) -> PathBuf {
    let mut project_path = current_dir().unwrap();
    project_path.push(project_name);
    project_path
}

pub fn parse_cli() -> Cli {
    Cli::parse()
}

pub fn create_new_config() {
    match create_dir(config_dir_path()) {
        Ok(_) => println!("[✓] Created configuration folder"),
        Err(err) => match err.kind() {
            // If the folder already exists it will just continue.
            ErrorKind::AlreadyExists => {
                println!("[✓] Configuration folder found");
            }
            other => panic!("[X] {other}"),
        },
    }

    let mut new_config_file =
        File::create(config_file_path()).expect("[!] Failed to create config file");
    new_config_file
        .write_all(DEFAULT_CONFIG.as_bytes())
        .expect("[!] Failed to write to config file");
    println!("[✓] Created config file");
}

fn read_config_string(cli: &Cli) -> (String, bool) {
    let config_file_path = config_file_path();
    match read_to_string(&config_file_path) {
        Ok(config) => (config, true),
        Err(err) => {
            // If it doesn't find the config folder it will check if the user intends to
            // create one. Otherwise it just warns the user.
            match err.kind() {
                ErrorKind::NotFound => {
                    // This check is made at this stage to be safe. Jector will only create a
                    // config file if it doesn't exist. If a user runs 'jector new config' but the
                    // config file already exists, it won't attempt to create a new one. It will
                    // attempt to create a project instead.
                    if cli.temp == "new" && cli.name == "config" {
                        create_new_config();
                    } else {
                        println!("[!] Configuration file not found\n[?] Run 'jector new config' to generate a new config file");
                    }
                }
                ErrorKind::IsADirectory => {
                    println!("[!] jector.toml is a directory, not a file!");
                }
                _ => panic!("[X] {err} {:?}", config_file_path),
            }
            // Since the new config file is empty, there's no need to read it. This is why it
            // returns an empty string.
            (String::new(), false)
        }
    }
}

pub fn read_config_struct(cli: &Cli) -> (Config, String, bool) {
    let (config_string, config_file_exists) = read_config_string(cli);
    let config_struct = match toml::from_str(&config_string) {
        Ok(conf) => conf,
        Err(err) => {
            match err.line_col() {
                Some((line, column)) => {
                    println!("[X] Error parsing config at line {line}, column {column}");
                }
                None => println!("[X] Could not parse config"),
            }
            // Since config hasn't been parsed correctly, an empty config will be returned
            Config {
                template: None,
                script: None,
                shell: if cfg!(target_os = "windows") {
                    String::from("cmd")
                } else {
                    String::from("sh")
                },
            }
        }
    };
    return (config_struct, config_string, config_file_exists)
}

pub const fn create_type(cli: &Cli, config_file_exists: bool) -> Create {
    if cli.git && !(cli.dir || cli.predef) && config_file_exists {
        Create::GitTemp
    } else if cli.dir && !(cli.git || cli.predef) && config_file_exists {
        Create::DirTemp
    } else if cli.predef && !(cli.git || cli.dir) && config_file_exists {
        Create::PredefScript
    } else if !(cli.git || cli.dir || cli.predef) && config_file_exists {
        Create::Project
    } else {
        Create::None
    }
}

pub fn project_type(
    git: Option<String>,
    project_path: &PathBuf,
    name: &str,
    path: Option<String>,
) -> Project {
    if Path::new(&project_path).is_dir() || Path::new(&project_path).is_file() {
        println!("[!] \"{}\" already exists", &name);
        Project::None
    } else if let Some(temp) = git {
        Project::Git(temp)
    } else if let Some(temp) = path {
        Project::Dir(temp)
    } else {
        println!("[!] Config file has no template linked");
        Project::None
    }
}

pub fn create_project(conf: Config, cli: &Cli) {
    let mut scripts = HashMap::new();
    if let Some(predef_script_entries) = conf.script {
        for entry in predef_script_entries {
            scripts.insert(entry.name, entry.run);
        }
    }
    let mut run: Vec<&str> = Vec::new();

    if let Some(templates) = conf.template {
        let mut found_template = false;
        // Look at the different templates in the config file
        for temp in templates {
            // Check if the name of the current template is the one to be created
            if temp.name == cli.temp {
                let mut found_script;
                // If a script has been specified via CLI
                if let Some(ref cli_scripts) = cli.scripts {
                    // Look though the available scripts, and if it is on the map,
                    // add the path to the script to the run vector
                    for cli_script in cli_scripts {
                        found_script = false;
                        for (name, path) in &scripts {
                            if name == cli_script {
                                run.push(path);
                                found_script = true;
                                // No need to look through the map since what it was looking
                                // for has been found
                                break;
                            }
                        }

                        if !found_script {
                            println!("[!] No script called \"{cli_script}\" was found.");
                        }
                    }
                }

                // Same thing but with the config file
                if let Some(temp_scripts) = temp.scripts {
                    for temp_script in temp_scripts {
                        found_script = false;
                        for (name, path) in &scripts {
                            if name == &temp_script {
                                run.push(path);
                                found_script = true;
                                break;
                            }
                        }

                        if !found_script {
                            println!("[!] No script called \"{}\" was found.", &temp_script);
                        }
                    }
                }

                found_template = true;

                let target_project_path = target_project_path(&cli.name);
                let create = project_type(temp.git, &target_project_path, &cli.name, temp.dir);
                match create {
                    Project::Git(git_repo_link) => {
                        create_git_project(
                            &git_repo_link,
                            &target_project_path,
                            &cli.name,
                            &run,
                            &conf.shell,
                        );
                    }
                    Project::Dir(template_dir_path) => {
                        create_dir_project(
                            &template_dir_path,
                            &target_project_path,
                            &cli.name,
                            &run,
                            &conf.shell,
                        );
                    }
                    Project::None => (),
                }
            }
        }

        if !found_template {
            println!("[!] Template \"{}\" not found", cli.temp);
        }
    } else {
        // If the config file is empty
        println!("[!] There aren't any config entries!\n[?] Edit the config file at ~/.config/jector/jector.toml");
    }
}

fn create_git_project(
    git_repo_link: &str,
    target_project_path: &PathBuf,
    project_name: &str,
    script_list: &Vec<&str>,
    shell: &str,
) {
    match Repository::clone(git_repo_link, target_project_path) {
        Ok(_) => {
            run_scripts(script_list, target_project_path, shell);
            println!("[✓] \"{project_name}\" created sucessfully");
        }
        Err(err) => println!("[!] Git failed: {err}"),
    };
}

fn create_dir_project(
    template_dir_path: &str,
    target_project_path: &Path,
    project_name: &str,
    script_list: &Vec<&str>,
    shell: &str,
) {
    if Path::new(&template_dir_path).is_dir() {
        // Recursively looks through the template directory. Only outputs paths
        // that could be accessed correctly.
        for entry in WalkDir::new(template_dir_path)
            .into_iter()
            .filter_map(std::result::Result::ok)
        {
            if entry.path().is_dir() {
                let mut cpd_dir = target_project_path.to_path_buf();
                cpd_dir.push(entry.path().strip_prefix(template_dir_path).unwrap());
                match create_dir_all(&cpd_dir) {
                    Ok(_) => (),
                    Err(err) => match err.kind() {
                        ErrorKind::AlreadyExists => (),
                        other => panic!("[X] {other}"),
                    },
                }
            } else if entry.path().is_file() {
                let mut cpd_file = target_project_path.to_path_buf();
                cpd_file.push(entry.path().strip_prefix(template_dir_path).unwrap());
                match copy(entry.path(), &cpd_file) {
                    Ok(_) => (),
                    Err(err) => match err.kind() {
                        ErrorKind::AlreadyExists => (),
                        other => panic!("[X] {other}"),
                    },
                }
            }
        }
        println!("[✓] \"{project_name}\" created sucessfully");
        run_scripts(script_list, target_project_path, shell);
    } else {
        println!("[!] Template directory does not exist");
    }
}

fn run_scripts(run: &Vec<&str>, project_dir: &Path, shell: &str) {
    for script in run {
        Command::new(shell)
            .current_dir(project_dir)
            .arg(script)
            .status()
            .expect("[!] Failed to execute script");
    }
}


pub fn add_template(
    header: &str,
    name: &str,
    template: &str,
    mut config: String,
    conf_path: &Path,
    temp_type: &str,
    scripts_opt: Option<Vec<String>>,
) {
    // Adds a `script = [SCRIPT_PATH]` to the bottom of the template if the user specified one
    let script = scripts_opt.map_or_else(String::new, |scripts| format!("script = [\"{}\"]\n", scripts.join("\", \"")));

    // Creates the content of the template
    let new_template = format!(
        "\n[[{header}]]\nname = \"{name}\"\n{temp_type} = \"{template}\"\n{script}");

    let mut conf = OpenOptions::new()
        .read(true)
        .write(true)
        .open(conf_path)
        .unwrap();

    config.push_str(&new_template);
    match conf.write_all(config.as_bytes()) {
        Ok(_) => println!("[✓] Added template to config file"),
        Err(err) => panic!("[X] {err}"),
    }
}
