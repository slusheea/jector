#![feature(io_error_more)]

mod jector;
use jector::{
    config_file_path, create_project, create_type, parse_cli,
    read_config_struct, Create, add_template,
};

fn main() {
    // config path = ~/.config/jector
    // config file = ~/.config/jector/jector.toml

    let config_file_path = config_file_path();
    let cli = parse_cli();

    // Read config file
    let (config_struct, config_string, config_file_exists) = read_config_struct(&cli);

    match create_type(&cli, config_file_exists) {
        Create::Project => {
            create_project(config_struct, &cli);
        }

        Create::GitTemp => {
            add_template(
                "template",
                &cli.name,
                &cli.temp,
                config_string,
                &config_file_path,
                "git",
                cli.scripts,
            );
        }

        Create::DirTemp => {
            add_template(
                "template",
                &cli.name,
                &cli.temp,
                config_string,
                &config_file_path,
                "dir",
                cli.scripts,
            );
        }

        Create::PredefScript => {
            add_template(
                "script",
                &cli.name,
                &cli.temp,
                config_string,
                &config_file_path,
                "run",
                None,
            );
        }

        Create::None => (),
    }
}

